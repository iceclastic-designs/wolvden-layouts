const Express = require('express');

const app = Express();
const port = 3009;

app.use(Express.static('styles'));

app.get('/', (req, res) => {
  res.send('Hello world!');
});

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
});
